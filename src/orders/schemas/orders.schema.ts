import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({ timestamps: true })
export class Order extends Document {
  @Prop({ required: true })
  phone: string;

  @Prop()
  name: string;

  @Prop()
  text: string;

  @Prop({ type: Types.ObjectId, ref: 'Plant', required: true })
  plant: string;

  @Prop({ default: 1, min: 1, validate: { validator: Number.isInteger } })
  amount: number;

  @Prop({ default: false })
  isPaid: boolean;
}

export const OrderSchema = SchemaFactory.createForClass(Order);
