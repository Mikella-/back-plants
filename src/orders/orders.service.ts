import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateOrderDto } from './dto/create-order.dto';
import { UpdateOrderDto } from './dto/update-order.dto';
import { Order } from './schemas/orders.schema';
import { Model } from 'mongoose';

@Injectable()
export class OrdersService {
  constructor(
    @InjectModel(Order.name) private readonly orderModel: Model<Order>,
  ) {}
  async create(createOrderDto: CreateOrderDto) {
    const createdOrder = await this.orderModel.create(createOrderDto);
    return createdOrder;
  }

  async findAll(unpaidOnly: boolean) {
    return this.orderModel
      .find(unpaidOnly ? { isPaid: false } : {})
      .populate('plant');
  }

  async markOrderAsPaid(id: string) {
    const order = await this.orderModel.findOne({ _id: id });
    if (!order) {
      throw new NotFoundException();
    }
    order.isPaid = true;
    return order.save();
  }

  update(id: string, updateOrderDto: UpdateOrderDto) {
    return this.orderModel.updateOne({ _id: id }, updateOrderDto);
  }
}
