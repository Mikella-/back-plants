import { Transform, Type } from 'class-transformer';
import {
  IsInt,
  IsMongoId,
  IsNotEmpty,
  IsPhoneNumber,
  IsString,
  Max,
  Min,
} from 'class-validator';

export class CreateOrderDto {
  @IsString()
  @IsNotEmpty()
  @IsPhoneNumber('KZ')
  phone: string;

  @IsString()
  name: string;

  @IsString()
  text: string;

  @IsString()
  @IsMongoId()
  @IsNotEmpty()
  plant: string;

  @IsInt()
  @IsNotEmpty()
  @Min(1)
  @Max(10000)
  amount: number;
}
