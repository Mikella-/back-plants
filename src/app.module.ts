import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { PlantsModule } from './plants/plants.module';
import { AuthMiddleware } from './auth.middleware';
import { CommentsModule } from './comments/comments.module';
import { OrdersModule } from './orders/orders.module';
import { UploadModule } from './upload/upload.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

@Module({
  imports: [
    MongooseModule.forRoot(
      'mongodb+srv://username:gucJ0wPGkEHA0cAy@cluster0.dki97kp.mongodb.net/?retryWrites=true&w=majority',
    ),
    PlantsModule,
    CommentsModule,
    OrdersModule,
    UploadModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'uploads'),
      renderPath: '/spa',
      serveRoot: '/uploads',
    }),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .forRoutes(
        { path: 'plants', method: RequestMethod.POST },
        { path: 'plants', method: RequestMethod.DELETE },
        { path: 'comments', method: RequestMethod.DELETE },
        { path: 'orders', method: RequestMethod.GET },
        { path: 'orders', method: RequestMethod.PUT },
      );
  }
}
