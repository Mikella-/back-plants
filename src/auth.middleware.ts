import {
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';

import * as bcrypt from 'bcrypt';

const encryptedPassword = bcrypt.hashSync(process.env.SECRET, 10);
@Injectable()
export class AuthMiddleware implements NestMiddleware {
  async use(req: any, res: any, next: () => void) {
    if (await bcrypt.compare(req.headers.authorization, encryptedPassword)) {
      next();
    } else {
      throw new UnauthorizedException();
    }
  }
}
