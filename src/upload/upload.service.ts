import { Injectable } from '@nestjs/common';

@Injectable()
export class UploadService {
  async upload(file: Express.Multer.File) {
    return {
      originalname: file.originalname,
      filename: file.filename,
    };
  }
}
