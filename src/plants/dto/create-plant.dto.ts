import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsString, Max, Min } from 'class-validator';

export class CreatePlantDto {
  @IsString()
  @IsNotEmpty()
  name: string;
  @IsString()
  @IsNotEmpty()
  image: string;
  @IsString()
  description: string;
  @Type(() => Number)
  @IsNumber()
  @Min(0)
  price: number;
  // @Type(() => [{ type: String, value: String }])
  @IsNotEmpty()
  attributes: [
    {
      name: string;
      nameRu: string;
      value: string;
    },
  ];
}
