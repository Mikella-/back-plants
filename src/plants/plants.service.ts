import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePlantDto } from './dto/create-plant.dto';
import { UpdatePlantDto } from './dto/update-plant.dto';
import { Model } from 'mongoose';
import { Plant } from './schemas/plants.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Order } from 'src/orders/schemas/orders.schema';
import { Comment } from 'src/comments/schemas/comments.schema';

@Injectable()
export class PlantsService {
  constructor(
    @InjectModel(Plant.name) private readonly plantsModel: Model<Plant>,
    @InjectModel(Order.name) private readonly ordersModel: Model<Order>,
    @InjectModel(Comment.name) private readonly commentsModel: Model<Comment>,
  ) {}
  async create(createPlantDto: CreatePlantDto) {
    const createdPlant = await this.plantsModel.create(createPlantDto);
    return createdPlant;
  }

  async findAll() {
    const allPlants = await this.plantsModel.find();
    return allPlants;
  }

  async findOne(id: string) {
    const foundPlant = await this.plantsModel.findOne({ _id: id });
    if (!foundPlant) {
      throw new NotFoundException();
    }
    return foundPlant;
  }

  async update(id: string, updatePlantDto: UpdatePlantDto) {
    const foundPlant = await this.plantsModel.findOne({ _id: id });
    if (!foundPlant) {
      throw new NotFoundException();
    }
    console.log(updatePlantDto.attributes);
    foundPlant.name = updatePlantDto.name;
    foundPlant.description = updatePlantDto.description;
    foundPlant.price = updatePlantDto.price;
    foundPlant.image = updatePlantDto.image;
    foundPlant.attributes = updatePlantDto.attributes;
    await foundPlant.save();
    return foundPlant;
  }

  async remove(id: string) {
    return [
      await this.plantsModel.remove({ _id: id }),
      await this.ordersModel.deleteMany({ plant: id }),
      await this.commentsModel.deleteMany({ plant: id }),
    ];
  }
}
