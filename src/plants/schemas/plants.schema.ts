import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';
@Schema({ _id: false })
class Attribute {
  @Prop()
  name: string;

  @Prop()
  nameRu: string;

  @Prop()
  value: string;
}
const AttributeSchema = SchemaFactory.createForClass(Attribute);
@Schema()
export class Plant extends Document {
  @Prop({ required: true })
  name: string;

  @Prop()
  image: string;

  @Prop()
  description: string;

  @Prop()
  price: number;

  @Prop({ type: [AttributeSchema] })
  attributes: Attribute[];
}

export const PlantSchema = SchemaFactory.createForClass(Plant);
