import { Module } from '@nestjs/common';
import { PlantsService } from './plants.service';
import { PlantsController } from './plants.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { PlantSchema } from './schemas/plants.schema';
import { OrderSchema } from 'src/orders/schemas/orders.schema';
import { CommentSchema } from 'src/comments/schemas/comments.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Plant', schema: PlantSchema },
      { name: 'Order', schema: OrderSchema },
      { name: 'Comment', schema: CommentSchema },
    ]),
  ],
  controllers: [PlantsController],
  providers: [PlantsService],
})
export class PlantsModule {}
