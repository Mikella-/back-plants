import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { CreateCommentDto } from './dto/create-comment.dto';
import { UpdateCommentDto } from './dto/update-comment.dto';
import { Comment } from './schemas/comments.schema';
import { Model } from 'mongoose';

@Injectable()
export class CommentsService {
  constructor(
    @InjectModel('Comment') private readonly commentModel: Model<Comment>,
  ) {}
  async create(createCommentDto: CreateCommentDto) {
    const createdComment = await this.commentModel.create(createCommentDto);
    return createdComment;
  }
  async findAllCommentsForModeration() {
    const allComments = await this.commentModel
      .find({})
      .populate('plant')
      .sort('-createdAt');
    return allComments;
  }
  async findAllCommentsByPlant(plant: string) {
    const allComments = await this.commentModel
      .find({ plant })
      .sort('-createdAt');
    return allComments;
  }

  async remove(id: string) {
    return this.commentModel.remove({ _id: id });
  }
}
