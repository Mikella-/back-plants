import { Transform } from 'class-transformer';
import { IsEmail, IsMongoId, IsNotEmpty, IsString } from 'class-validator';

export class CreateCommentDto {
  @IsString()
  @IsNotEmpty()
  @IsEmail()
  email: string;
  @IsString()
  @Transform(({ value }) => value.trim())
  @IsNotEmpty()
  text: string;
  @IsString()
  @IsNotEmpty()
  @IsMongoId()
  plant: string;
}
