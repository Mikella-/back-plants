import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

@Schema({ timestamps: true })
export class Comment extends Document {
  @Prop({ required: true })
  email: string;

  @Prop()
  text: string;

  @Prop({ type: Types.ObjectId, ref: 'Plant', required: true })
  plant: string;
}

export const CommentSchema = SchemaFactory.createForClass(Comment);
